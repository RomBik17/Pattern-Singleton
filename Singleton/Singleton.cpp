
#include <memory>
#include <iostream>

class singleton {
public:
    //delete copy operator
    singleton(const singleton&) = delete;
    singleton& operator =(const singleton&) = delete;

    static singleton& instance()
    {
        static singleton res;
        return res;
    }

    const unsigned& width()
    {
        return mScreenWidth;
    }

    const unsigned& height()
    {
        return mScreenHeight;
    }

    void load()
    {
        mScreenWidth = 1920;
        mScreenHeight = 1080;
    }

private:
    //hide constructor
    singleton() = default;

    unsigned mScreenWidth{ 0 };
    unsigned mScreenHeight{ 0 };
};

int main()
{
    singleton::instance().load();

    std::cout << singleton::instance().height() << std::endl;
    std::cout << singleton::instance().width() << std::endl;
}

